unit Task2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs;

type
  TForm1 = class(TForm)
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
    FStartPoint, FEndPoint: TPoint;
    FDrawingLine: boolean;
    bm: TBitmap;
    procedure AddLineToCanvas;
    procedure SwapBuffers;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
  Form1.color := clRed;
  bm := TBitmap.Create;
  FDrawingLine := false;
end;

procedure TForm1.FormMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  FStartPoint := Point(X, Y);
  FDrawingLine := true;
  if Button = mbLeft then
    Canvas.Pen.Color := clGreen
  else if Button = mbRight then
    Canvas.Pen.Color := clBlue;
end;

procedure TForm1.FormMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  if FDrawingLine then
  begin
    SwapBuffers;
    Canvas.MoveTo(FStartPoint.X, FStartPoint.Y);
    Canvas.LineTo(X, Y);
  end;
end;

procedure TForm1.FormMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  FDrawingLine := false;
  FEndPoint := Point(X, Y);
  AddLineToCanvas;
  SwapBuffers;
end;

procedure TForm1.AddLineToCanvas;
begin
  bm.Canvas.MoveTo(FStartPoint.X, FStartPoint.Y);
  bm.Canvas.LineTo(FEndPoint.X, FEndPoint.Y);
end;

procedure TForm1.FormPaint(Sender: TObject);
begin
  SwapBuffers;
end;

procedure TForm1.SwapBuffers;
begin
  BitBlt(Canvas.Handle, 0, 0, ClientWidth, ClientHeight,
    bm.Canvas.Handle, 0, 0, SRCCOPY);
end;

procedure TForm1.FormResize(Sender: TObject);
begin
  bm.Width:=ClientWidth;
  bm.Height:= ClientHeight;
end;
end.
