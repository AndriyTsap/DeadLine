unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Math, StdCtrls;

type
  TForm1 = class(TForm)
    lbl1: TLabel;
    edt1: TEdit;
    lbl2: TLabel;
    edt2: TEdit;
    lbl3: TLabel;
    btn1: TButton;
    lbl4: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure btn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
  lbl4.Caption:='';
end;

procedure TForm1.btn1Click(Sender: TObject);
var r,h,v : Real;
begin
  r:=StrToFloat(edt1.Text);
  h:=StrToFloat(edt2.Text);
  v:=Pi*r*r*h;
  lbl4.Caption:='���������:'+sLineBreak;
  lbl4.Caption:=lbl4.Caption+'V='+FloatToStr(v)+sLineBreak;
end;

end.
