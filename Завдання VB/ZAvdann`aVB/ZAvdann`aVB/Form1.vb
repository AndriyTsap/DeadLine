﻿Public Class Form1

    Public Class Student

        Public Name, BirthdayDate, City As String

        Public Sub New(ByVal _name As String, ByVal _birthdayDate As String, ByVal _city As String)
        Name = _name
        BirthdayDate = _birthdayDate
        City = _city
                
    End Sub

    End Class

    Dim studentsList As New List(Of Student)()
    Dim IFStudentsList As New List(Of Student)()
    Dim NotIFStudentsList As New List(Of Student)()

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        

        studentsList.Add(New Student("Блищак Д.", "22.03.1998","Сан-Франциско"))
	    studentsList.Add(New Student("Блищак Е.", "22.03.1998","Сан-Франциско"))
        studentsList.Add(New Student("Боднар І.", "05.04.1998","Сан-Франциско"))
        studentsList.Add(New Student("Дадяк М.", "14.03.1998","Сан-Франциско"))
        studentsList.Add(New Student("Коваль В.", "01.03.1998","Сан-Франциско"))
        studentsList.Add(New Student("Микула В.", "02.06.1998","Сан-Франциско"))
        studentsList.Add(New Student("Ожгевич О.", "02.11.1998","Івано-Франківськ"))
        studentsList.Add(New Student("Савчин О.", "23.05.1998","Сан-Франциско"))
        studentsList.Add(New Student("Савчин О.", "23.05.1998","Сан-Франциско"))
        studentsList.Add(New Student("Сворак А.", "04.03.1998","Сан-Франциско"))
        studentsList.Add(New Student("Соколюк Б.", "20.05.1997","Сан-Франциско"))
        studentsList.Add(New Student("Таргонін В.", "24.04.1998","Івано-Франківськ"))
        studentsList.Add(New Student("Фурта Н.", "11.08.1998","Сан-Франциско"))
        studentsList.Add(New Student("Юсеф Омар", "14.08.1993","Сан-Франциско"))

        For Each student As Student In studentsList
            If String.Compare(student.City,"Івано-Франківськ") Then
                IFStudentsList.Add(student)
            Else 
                NotIFStudentsList.Add(student)    
            End If
	    Next

        RadioButton1.Select

    End Sub

    Private Sub RadioButton1_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton1.CheckedChanged

        TextBox1.Text = ""
        
        For Each student As Student In studentsList
	        TextBox1.Text += student.Name +"  "+ student.BirthdayDate +"  "+ student.City+vbCrLf
        Next

    End Sub

    Private Sub RadioButton2_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton2.CheckedChanged

        TextBox1.Text = ""
        
        For Each student As Student In NotIFStudentsList
	        TextBox1.Text += student.Name +"  "+ student.BirthdayDate +"  "+ student.City+vbCrLf
        Next

    End Sub

    Private Sub RadioButton3_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton3.CheckedChanged

        TextBox1.Text = ""
        
        For Each student As Student In IFStudentsList
	        TextBox1.Text += student.Name +"  "+ student.BirthdayDate +"  "+ student.City+vbCrLf
        Next

    End Sub
End Class
