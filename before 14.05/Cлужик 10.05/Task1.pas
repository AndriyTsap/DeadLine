unit Task1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Math;

type
  TForm1 = class(TForm)
    lbl1: TLabel;
    edt1: TEdit;
    btn1: TButton;
    lbl2: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure btn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
  lbl2.Caption:='';
end;

procedure TForm1.btn1Click(Sender: TObject);
  var aString: string;
  var a :Integer;
  var s :Real;
begin
  aString:=edt1.Text;
  a:=strtoint(aString);
  s:=a*a*(Power(3, (1/2))/4);
  lbl2.Caption:='����� ����������:'+FloatToStr(s);
end;

end.
