unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Math;

type
  TForm1 = class(TForm)
    lbl1: TLabel;
    lbl4: TLabel;
    edt1: TEdit;
    edt2: TEdit;
    Edit1: TEdit;
    btn1: TButton;
    lbl2: TLabel;
    procedure btn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.btn1Click(Sender: TObject);
var a,b,c : Integer;
var d,x1,x2 : Double;
begin
  a:=StrToInt(edt1.Text);
  b:=StrToInt(edt2.Text);
  c:=StrToInt(edit1.Text);

  lbl2.Caption:=IntToStr(a);
  lbl2.Caption:=lbl2.Caption+'x^2+'+IntToStr(b)+'x+'+IntToStr(c)+'=0'+sLineBreak;

  d:=b*b-4*a*c;
  if d>0 then
  begin
    x1:=(-b-Sqrt(d))/(2*a);
    x2:=(-b+Sqrt(d))/(2*a);
  end
  else Form1.Close;


  lbl2.Caption:=lbl2.Caption+'d='+FloatToStr(d)+sLineBreak;
  lbl2.Caption:=lbl2.Caption+'x1='+FloatToStr(x1)+sLineBreak;
  lbl2.Caption:=lbl2.Caption+' x2='+FloatToStr(x2)+sLineBreak;
end;

end.
